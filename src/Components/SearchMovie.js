// import React, { useState } from "react";
import React from "react";
import search from "../Assets/search.png";
import "../Styles/SearchMovie.css";

export default function SearchMovie() {
  // const SearchMovie = (props) => {
  //     const [search, setSearch] = useState("");
  //     const [searchResult, setSearchResult] = useState('')

  //     const handleSearch = () => {
  //         let searchResult = props.data.find((item) => item.name === search)

  //         if (searchResult) {
  //             setSearchResult(searchResult)
  //         } else {
  //             setSearchResult('')
  //         }
  //         setSearch('')
  //     }
  //   };
  return (
    <div>
      <div className="search-form">
        <table className="element-container">
          <tr>
            <td>
              <input type="text" className="search-input" />
            </td>
            <td>
              <button className="search-button">
                {" "}
                <img className="search-logo" src={search} alt="search" />
              </button>
            </td>
          </tr>
        </table>
        {/* <button>Search Movie</button> */}
      </div>

      {/* <div className="search-result">
          {searchResult !== "" ? (
              <div>Ada Search</div>
          ) : (
              <div>Tidak Ada Search</div>
          )}
      </div> */}
    </div>
  );
}
