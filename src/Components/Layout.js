import React, { Children, Fragment, useState } from 'react'
import Modal from 'react-modal'

import SearchMovie from './SearchMovie'
import logo from '../Assets/movie.png'
import '../Styles/Navbar.css'

export const MODAL_LOGIN = 1
export const MODAL_SIGNUP = 2

export default function Layout({ children }) {
   const [isModalOpen, setIsModalOpen] = useState(false)
   const [whichModal, setWhichModal] = useState(null)
   const [userData, setUserData] = useState({ name: '', email: '', password: '' })

   return (
      <Fragment>
         <div className='main-container'>
            <div className='logo'>
               <div>
                  <a href='/'>
                     <img className='logo-img' src={logo} alt='logo' />
                  </a>
               </div>
               <div className='logo-name'>Milan TV</div>
               <SearchMovie />
            </div>
            <div>
               <button
                  onClick={() => {
                     setIsModalOpen(true)
                     setWhichModal(MODAL_SIGNUP)
                  }}
                  className='navbar-button'>
                  Sign Up
               </button>
            </div>
         </div>

         <main>{children}</main>

         <footer>
            <h1>INI FOOTER</h1>
         </footer>

         <Modal isOpen={isModalOpen} onRequestClose={() => setIsModalOpen(false)} className='modal-container' overlayClassName='modal-overlay-center' contentLabel='Sign In'>
            {renderWhichModal()}
         </Modal>
      </Fragment>
   )

   function renderWhichModal() {
      const handleLogin = (event) => {
         event.preventDefault()
         setIsModalOpen(false)
      }

      const handleChange = (event) => {
         setUserData({
            ...userData,
            [event.target.name]: event.target.value,
         })
      }

      const submitSignUp = (event) => {
         event.preventDefault()
         //code below for fetching
      }

      const sumbitSignIn = (event) => {
         event.preventDefault()
         //code below for fetching
      }

      switch (whichModal) {
         case MODAL_SIGNUP:
            return (
               <div className='home-signup'>
                  <div className='title-wrap'>
                     <img className='logo-signup' src={logo} alt='logo' />
                     <div>MilanTV</div>
                  </div>
                  <form onSubmit={handleLogin} className='home-login-form'>
                     <div>Full Name</div>
                     <input className='form-input' type='text' placeholder='Full Name' name='name' onChange={(event) => handleChange(event)} />
                     <div>Email</div>
                     <input className='form-input' type='email' placeholder='Email' name='email' onChange={(event) => handleChange(event)} />
                     <div>Password</div>
                     <input className='form-input' type='password' placeholder='Password' name='password' onChange={(event) => handleChange(event)} />
                     <button onClick={submitSignUp} type='submit' className='home-login-button'>
                        Sign Up
                     </button>
                     <div className='redirect'>
                        Already have an account?
                        <span onClick={() => setWhichModal(MODAL_LOGIN)}>Log In</span>
                     </div>
                  </form>
               </div>
            )
         case MODAL_LOGIN:
            return (
               <div className='home-signup'>
                  <h1>SIGN IN</h1>
                  <form onSubmit={handleLogin} className='home-login-form'>
                     <div>Email</div>
                     <input className='form-input' type='email' placeholder='Email' name='email' onChange={(event) => handleChange(event)} />
                     <div>Password</div>
                     <input className='form-input' type='password' placeholder='Password' name='password' onChange={(event) => handleChange(event)} />
                     <button onClick={sumbitSignIn} type='submit' className='home-login-button'>
                        Sign In
                     </button>
                     <div className='redirect'>
                        <span onClick={() => setWhichModal(MODAL_SIGNUP)}>Create an account</span>
                     </div>
                  </form>
               </div>
            )
         default:
            break
      }
   }
}
