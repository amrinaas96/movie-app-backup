import React from "react";
import Layout from "../Components/Layout";
import CarouselHome from "../Components/CarouselHome";

import { checkLogin } from "../Helper";

export default function Home() {
  return (
    <Layout>
      <CarouselHome />
      <div>{checkLogin() ? "Sudah Login" : "Belum Login"}</div>
    </Layout>
  );
}
